﻿using PainelEletronico.API.Models;
using PainelEletronicoWeb.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace PainelEletronico.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")] // tune to your needs
    public class SessoesVotacaoController : ApiController
    {
        private paineleletronicoEntities db = new paineleletronicoEntities();
        [ResponseType(typeof(SessoesVotacaoResponse))]
        public IHttpActionResult GetSessaoAberta()
        {
            var votacao = new SessoesVotacaoResponse();
            sessoes_votacao sessoesVotacao = db.sessoes_votacao.FirstOrDefault(x => x.Status == "ABERTA");
            if(sessoesVotacao != null)
            {
                votacao.Id = sessoesVotacao.ID;
                votacao.Descricao = sessoesVotacao.Descricao;
                votacao.OpcoesVoto = sessoesVotacao.OpcoesVoto;
            }            

            return Ok(votacao);
        }
    }
}
