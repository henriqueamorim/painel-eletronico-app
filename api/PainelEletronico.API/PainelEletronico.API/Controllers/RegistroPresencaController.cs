using PainelEletronicoWeb.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace PainelEletronico.API.Controllers
{
  [EnableCors(origins: "*", headers: "*", methods: "*")]
  public class RegistroPresencaController : ApiController
  {
    private paineleletronicoEntities db = new paineleletronicoEntities();

    [ResponseType(typeof(bool))]

    public IHttpActionResult Post(registro_presenca presenca)
    {
     

      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      db.registro_presenca.Add(presenca);

      try
      {
        db.SaveChanges();
      }
      catch (DbUpdateException ex)
      {
        return BadRequest();

      }

      return Ok(true);
    }
  }
}
