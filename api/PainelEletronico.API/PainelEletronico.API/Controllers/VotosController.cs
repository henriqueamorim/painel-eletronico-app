using PainelEletronico.API.Helper;
using PainelEletronico.API.Models;
using PainelEletronicoWeb.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace PainelEletronico.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class VotosController : ApiController
    {
        private paineleletronicoEntities db = new paineleletronicoEntities();

        // POST: api/User
        [ResponseType(typeof(bool))]
        
        public IHttpActionResult Post(VotosModelRequest voto)
        {
            var votoModel = new votos();

            votoModel.IDSessao = voto.IDSessao;
            votoModel.IDUsuario = voto.IDUsuario;
            votoModel.voto = new RemoverAcentos().RemoveAccents(voto.voto);
            votoModel.usuarios = db.usuarios.FirstOrDefault(x => x.ID == voto.IDUsuario);
            votoModel.sessoes_votacao = db.sessoes_votacao.FirstOrDefault(x => x.ID == voto.IDSessao);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.votos.Add(votoModel);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                return BadRequest();

            }

            return Ok(true);
        }
    }
}
