﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;
using PainelEletronico.API.Models;
using PainelEletronicoWeb.EntityFramework;

namespace PainelEletronico.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")] // tune to your needs

    public class UserController : ApiController
    {
        private paineleletronicoEntities db = new paineleletronicoEntities();
        
        // GET: api/User
        public IHttpActionResult Getusuarios()
        {
            List<usuarios> usuarios = db.usuarios.ToList();
            var listUser = new List<UserResponse>();
            foreach (var item in usuarios)
            {
                var user = new UserResponse();
                user.Id = item.ID;
                user.Nome = item.Nome;
                user.Partido = item.partidos.Nome;
                //user.Foto = item.Foto;
                var camara = db.camaras_municipais.FirstOrDefault(x => x.ID == item.IDCamaraMunicipal);
                user.Brasao = camara.Brasao;
                user.Camara = camara.Descricao;
                user.Estado = camara.Estado;
                listUser.Add(user);
            }
            
            return Ok(listUser);

           //return db.usuarios;

        }

        // GET: api/User/5
        [ResponseType(typeof(UserResponse))]
        public IHttpActionResult Getusuarios(Guid id)
        {
           // List<camaras_municipais> camaras = db.camaras_municipais.Where(x => x.Estado == "SP").ToList();
            usuarios usuarios = db.usuarios.Find(id);
            if (usuarios == null)
            {
                return NotFound();
            }
            var user = new UserResponse();
            user.Email = usuarios.Email;
            user.Id = usuarios.ID;
            user.Nome = usuarios.Nome;
            user.Partido = usuarios.partidos.Nome;
            user.Foto = usuarios.Foto ;
            var camara = db.camaras_municipais.FirstOrDefault(x => x.ID == usuarios.IDCamaraMunicipal);
            user.Brasao = camara.Brasao;
            user.Camara = camara.Descricao;
            user.Estado = camara.Estado;
            user.Diretorio = camara.Diretorio;
            return Ok(user);
        }

        // PUT: api/User/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putusuarios(Guid id, usuarios usuarios)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != usuarios.ID)
            {
                return BadRequest();
            }

            db.Entry(usuarios).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!usuariosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/User
        [ResponseType(typeof(usuarios))]
        public IHttpActionResult Postusuarios(usuarios usuarios)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.usuarios.Add(usuarios);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (usuariosExists(usuarios.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = usuarios.ID }, usuarios);
        }

        // DELETE: api/User/5
        [ResponseType(typeof(usuarios))]
        public IHttpActionResult Deleteusuarios(Guid id)
        {
            usuarios usuarios = db.usuarios.Find(id);
            if (usuarios == null)
            {
                return NotFound();
            }

            db.usuarios.Remove(usuarios);
            db.SaveChanges();

            return Ok(usuarios);
        }

        [ResponseType(typeof(bool))]
        [Route("api/User/Login")]
        public IHttpActionResult PostLogin(LoginRequest login)
        {

            usuarios usuarios = db.usuarios.FirstOrDefault(x => x.ID == login.Id && x.Senha == login.Senha);
            if (usuarios == null)
            {
                return NotFound();
            }
            return Ok(true);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool usuariosExists(Guid id)
        {
            return db.usuarios.Count(e => e.ID == id) > 0;
        }
    }
}