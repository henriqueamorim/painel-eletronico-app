﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PainelEletronico.API.Models
{
    public class SessoesVotacaoResponse
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public string OpcoesVoto { get; set; }
    }
}