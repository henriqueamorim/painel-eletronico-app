﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PainelEletronico.API.Models
{
    public class UserResponse
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Partido { get; set; }
        public byte[] Foto { get; set; }
        public byte[] Brasao { get; set; }
        public string Camara { get; set; }
        public string Estado { get; set; }
        public string Diretorio { get; set; }

    }
}