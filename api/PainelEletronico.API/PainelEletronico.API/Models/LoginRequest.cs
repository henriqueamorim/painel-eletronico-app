﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PainelEletronico.API.Models
{
    public class LoginRequest
    {
        public Guid Id { get; set; }

        public string Senha { get; set; }
    }
}