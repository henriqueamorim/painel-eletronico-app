﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PainelEletronico.API.Models
{
    public class VotosModelRequest
    {
        public Guid IDSessao { get; set; }
        public Guid IDUsuario { get; set; }
        public string voto { get; set; }
    }
}