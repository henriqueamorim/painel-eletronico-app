import { PresencaModel } from './../service/model/presenca-model';
import { LoginRequest } from './../service/model/login-model';
import { Users } from './../service/model/user-model';
import { UserService } from './../service/user.service';
import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { listLazyRoutes } from '@angular/compiler/src/aot/lazy_routes';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  public users: Users[];
  public user: Users;
  public imageFoto;
  public imageBrasao;
  public login: LoginRequest;
  public senha: string = "";
  public valor: string;

  constructor(private service: UserService,
    private _sanitizer: DomSanitizer,
    private router: Router,
    public alertController: AlertController,
    private nativeStorage: NativeStorage
    )
    {
    this.service.listUser().subscribe((item: Users[]) => {
      this.users = item;
    });
  }
  buscarUser(event) {
    let id = event.target.value
    this.service.findUser(id).subscribe((item) => {
      this.user = item;
      this.imageFoto = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
                 + this.user.Foto);
                 this.imageBrasao = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
                 + this.user.Brasao);
      console.log(item)
    });
  }
  async loginUser(){
    this.login = new LoginRequest();
    this.login.Id = this.user.Id;
    this.login.Senha = this.senha;
    this.service.loginUser(this.login).subscribe((x) => {
      debugger
    this.registrarPresenca();

      //
  //     this.nativeStorage.setItem('user', this.user)
  // .then(
  //   () => this.router.navigate(['/tabs/tab2']) ,
  //   error => console.error('Error storing item', error)
  // );

      
    },async (e)=>{
      await this.presentAlert();
    })
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Alert',
      subHeader: 'Erro',
      message: 'Usuario e senha invalido.',
      buttons: ['OK']
    });    

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  } 

  registrarPresenca(){
    var presenca = new PresencaModel();
    presenca.Id = this.user.Id;
    presenca.DataPresenca = new Date();    
    this.service.registroPresenca(presenca).subscribe((x) => {
      localStorage.setItem("id",this.user.Id);
      this.router.navigate(['/tabs/tab2']) 
    }
    )    
  }

  pegarValor(valorBtn){    
    this.senha += valorBtn;
  }
  
  limpar() {
    this.senha = ""; 
  } 

}