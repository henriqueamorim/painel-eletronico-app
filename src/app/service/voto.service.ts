import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { VotoModel } from "./model/voto-model";

@Injectable({
    providedIn: 'root'
  })
  export class VotoService {
  
    constructor(public httpClient: HttpClient) { }        

    confirmaVoto(voto : VotoModel){
        return this.httpClient.post<any>(environment.api + "api/Votos", voto)
      }   
    }