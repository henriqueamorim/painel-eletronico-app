import { LoginRequest } from './model/login-model';
import { environment } from './../../environments/environment';
import { Users } from './model/user-model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PresencaModel } from './model/presenca-model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(public httpClient: HttpClient) { }

  listUser() {
    return this.httpClient.get<Users[]>(environment.api + "api/User");
  }
  findUser(id){
    return this.httpClient.get<any>(environment.api + "api/User/" + id);
  }
  loginUser(login :LoginRequest){
    return this.httpClient.post<any>(environment.api + "api/User/Login", login)
  }

  registroPresenca(Presenca : PresencaModel){
    return this.httpClient.post<any>(environment.api + "api/RegistroPresenca", Presenca)
  }
}
