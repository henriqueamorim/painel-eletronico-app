import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { SessaoVotacaoRequest } from "./model/sessao-model";

@Injectable({
    providedIn: 'root'
  })
  export class SessaoService {
  
    constructor(public httpClient: HttpClient) { }  
    
    getSessao(){
      return this.httpClient.get<any>(environment.api + "api/SessoesVotacao")
    }
  }