export class Users {
    Id:      string;
    Nome:    string;
    Email:   string;
    Partido: string;
    Foto:    string;
    Brasao:  string;
    Camara:  string;
    Estado:  string;
    Diretorio: string;
}