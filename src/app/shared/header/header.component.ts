import { Component, OnInit } from '@angular/core';
import { Users } from 'src/app/service/model/user-model';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  public users: Users[];
  public user: Users;
  public imageFoto;
  public imageBrasao;

  constructor(private service: UserService) {
    
   }

  ngOnInit() {
    this.service.listUser().subscribe((item: Users[]) => {
      this.users = item;
    });
  }

}
