import { SessaoVotacaoRequest } from './../service/model/sessao-model';
import { VotoModel } from './../service/model/voto-model';
import { SessaoService } from './../service/sessao.service';
import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Users } from '../service/model/user-model';
import { UserService } from '../service/user.service';
import { VotoService } from '../service/voto.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit{

  public users: Users[];
  public user: Users;
  public imageFoto;
  public imageBrasao;
  public sessao: SessaoVotacaoRequest;
  public concordo: boolean;
  public discordo: boolean;
  public abster: boolean;
  public sessaoAberta: boolean;
  public votoModel= new VotoModel();
  
  constructor(private service: UserService,
    private _sanitizer: DomSanitizer,
    private router: Router,
    private sessaoService: SessaoService,
    public alertController: AlertController,
    public services: VotoService) {
    
  }
  ngOnInit(): void {
    let id = localStorage.getItem("id");
    this.service.findUser(id).subscribe((item) => {
      this.user = item;
      this.imageFoto = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
                 + this.user.Foto);
                 this.imageBrasao = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
                 + this.user.Brasao);
      console.log(item)        
    });
    
      this.sessaoService.getSessao().subscribe((item) => {
        if(item.Descricao != null){
        console.log(item?.OpcoesVoto?.split('/'));
        this.sessao = item;
        this.sessaoAberta = true;  
        }
        else{
          this.sessaoAberta = false;
        }      
      }) 
  }
  
  buscarUser(event) {
    let id = event.target.value
    this.service.findUser(id).subscribe((item) => {
      this.user = item;
      this.imageFoto = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
                 + this.user.Foto);
                 this.imageBrasao = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
                 + this.user.Brasao);
      console.log(item)
    });
  }  
  mudarCorConcordo(){
     this.concordo = true;
     this.discordo = false;
     this.abster = false;
    }
  mudarCorDiscordo(){
      this.discordo = true;
      this.concordo = false;
     this.abster = false;
    } 
  mudarCorAbster(){
      this.abster = true;
      this.discordo = false;
      this.concordo = false;
    }  
    
  confirmaVoto(valor,tipo){
    switch (tipo) {
      case 1:
        this.mudarCorConcordo();
        break;
        case 2:
        this.mudarCorDiscordo();
        break;
        case 3:
        this.mudarCorAbster();
        break;
    }
    this.votoModel.voto = valor;
    this.votoModel.IDSessao = this.sessao.Id;
    this.votoModel.IDUsuario= localStorage.getItem("id");


    this.services.confirmaVoto(this.votoModel).subscribe(() =>{
      alert("Voto confirmado com sucesso.")
    })
  }
}
