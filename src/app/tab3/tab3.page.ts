import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Users } from '../service/model/user-model';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  public users: Users[];
  public user: Users;
  public imageFoto;
  public imageBrasao;
  
  constructor(private service: UserService,
    private _sanitizer: DomSanitizer,
    private router: Router,
    public alertController: AlertController) {
      let id = localStorage.getItem("id");
      this.service.findUser(id).subscribe((item) => {
        this.user = item;
        this.imageFoto = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
                   + this.user.Foto);
                   this.imageBrasao = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
                   + this.user.Brasao);
        console.log(item)
      });

  }
  buscarUser(event) {
    let id = event.target.value
    this.service.findUser(id).subscribe((item) => {
      this.user = item;
      this.imageFoto = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
                 + this.user.Foto);
                 this.imageBrasao = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
                 + this.user.Brasao);
      console.log(item)
    });
  }

}